<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A62714">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the brazen-head</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A62714 of text R22757 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing T1389A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A62714</idno>
    <idno type="STC">Wing T1389A</idno>
    <idno type="STC">ESTC R22757</idno>
    <idno type="EEBO-CITATION">12364760</idno>
    <idno type="OCLC">ocm 12364760</idno>
    <idno type="VID">60379</idno>
    <idno type="PROQUESTGOID">2240975868</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A62714)</note>
    <note>Transcribed from: (Early English Books Online ; image set 60379)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 900:12)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the brazen-head</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1685?]</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in Huntington Library.</note>
      <note>Appeal to "... change nothing of religion nor the laws, then no discontent shall here invade."</note>
      <note>Broadside.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Freedom of religion -- England -- Early works to 1800.</term>
     <term>Broadsides</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>To the brazen-head.</ep:title>
    <ep:author>[no entry]</ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>341</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-01</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-02</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change><date>2008-02</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A62714-t">
  <body xml:id="A62714-e0">
   <div type="poem" xml:id="A62714-e10">
    <pb facs="tcp:60379:1" xml:id="A62714-001-a"/>
    <head xml:id="A62714-e20">
     <w lemma="to" pos="prt" xml:id="A62714-001-a-0010">To</w>
     <w lemma="the" pos="d" xml:id="A62714-001-a-0020">the</w>
     <w lemma="brazenhead" pos="n1" reg="Brazenhead" xml:id="A62714-001-a-0030">Brazen-Head</w>
     <pc unit="sentence" xml:id="A62714-001-a-0040">.</pc>
    </head>
    <lg xml:id="A62714-e30">
     <l xml:id="A62714-e40">
      <w lemma="what" pos="crq" xml:id="A62714-001-a-0050">WHat</w>
      <w lemma="strepitantious" pos="j" xml:id="A62714-001-a-0060">Strepitantious</w>
      <w lemma="noise" pos="n1" xml:id="A62714-001-a-0070">Noise</w>
      <w lemma="be" pos="vvz" xml:id="A62714-001-a-0080">is</w>
      <w lemma="it" pos="pn" xml:id="A62714-001-a-0090">it</w>
      <w lemma="that" pos="cs" xml:id="A62714-001-a-0100">that</w>
      <w lemma="sound" pos="vvz" xml:id="A62714-001-a-0110">sounds</w>
      <pc xml:id="A62714-001-a-0120">,</pc>
     </l>
     <l xml:id="A62714-e50">
      <w lemma="from" pos="acp" xml:id="A62714-001-a-0130">From</w>
      <w lemma="raise" pos="j-vn" xml:id="A62714-001-a-0140">Raised</w>
      <w lemma="bank" pos="n2" xml:id="A62714-001-a-0150">Banks</w>
      <pc xml:id="A62714-001-a-0160">,</pc>
      <w lemma="or" pos="cc" xml:id="A62714-001-a-0170">or</w>
      <w lemma="from" pos="acp" xml:id="A62714-001-a-0180">from</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-0190">the</w>
      <w lemma="low" pos="jc" xml:id="A62714-001-a-0200">Lower</w>
      <w lemma="ground" pos="n2" xml:id="A62714-001-a-0210">Grounds</w>
      <pc unit="sentence" xml:id="A62714-001-a-0220">?</pc>
     </l>
     <l xml:id="A62714-e60">
      <w lemma="from" pos="acp" xml:id="A62714-001-a-0230">From</w>
      <w lemma="hollow" pos="j" xml:id="A62714-001-a-0240">Hollow</w>
      <w lemma="cavern" pos="n2" xml:id="A62714-001-a-0250">Caverns</w>
      <pc xml:id="A62714-001-a-0260">,</pc>
      <w lemma="labyrinth" pos="n2" xml:id="A62714-001-a-0270">Labyrinths</w>
      <w lemma="from" pos="acp" xml:id="A62714-001-a-0280">from</w>
      <w lemma="afar" pos="av" xml:id="A62714-001-a-0290">afar</w>
      <pc xml:id="A62714-001-a-0300">,</pc>
     </l>
     <l xml:id="A62714-e70">
      <w lemma="threaten" pos="vvg" reg="Threatening" xml:id="A62714-001-a-0310">Threatning</w>
      <w lemma="confusion" pos="n2" xml:id="A62714-001-a-0320">Confusions</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0330">of</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-0340">a</w>
      <w lemma="dreadful" pos="j" xml:id="A62714-001-a-0350">Dreadful</w>
      <w lemma="war" pos="n1" xml:id="A62714-001-a-0360">War</w>
      <pc unit="sentence" xml:id="A62714-001-a-0370">?</pc>
     </l>
     <l xml:id="A62714-e80">
      <w lemma="what" pos="crq" xml:id="A62714-001-a-0380">What</w>
      <w lemma="dismal" pos="j" xml:id="A62714-001-a-0390">Dismal</w>
      <w lemma="cry" pos="vvz" xml:id="A62714-001-a-0400">Cries</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0410">of</w>
      <w lemma="people" pos="n1" xml:id="A62714-001-a-0420">people</w>
      <w lemma="in" pos="acp" xml:id="A62714-001-a-0430">in</w>
      <w lemma="despair" pos="n1" xml:id="A62714-001-a-0440">Despair</w>
     </l>
     <l xml:id="A62714-e90">
      <w lemma="fill" pos="vvb" xml:id="A62714-001-a-0450">Fill</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-0460">the</w>
      <w lemma="vast" pos="j" xml:id="A62714-001-a-0470">vast</w>
      <w lemma="region" pos="n1" xml:id="A62714-001-a-0480">Region</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0490">of</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-0500">the</w>
      <w lemma="trouble" pos="j-vn" xml:id="A62714-001-a-0510">Troubled</w>
      <w lemma="air" pos="n1" xml:id="A62714-001-a-0520">Air</w>
      <pc unit="sentence" xml:id="A62714-001-a-0530">?</pc>
     </l>
     <l xml:id="A62714-e100">
      <w lemma="the" pos="d" xml:id="A62714-001-a-0540">The</w>
      <w lemma="tune" pos="n1" xml:id="A62714-001-a-0550">Tune</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0560">of</w>
      <w lemma="horror" pos="n1" reg="Horror" xml:id="A62714-001-a-0570">Horrour</w>
      <pc xml:id="A62714-001-a-0580">,</pc>
      <w lemma="or" pos="cc" xml:id="A62714-001-a-0590">or</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0600">of</w>
      <w join="right" lemma="what" pos="crq" xml:id="A62714-001-a-0610">what</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A62714-001-a-0611">'s</w>
      <w lemma="as" pos="acp" xml:id="A62714-001-a-0620">as</w>
      <w lemma="strange" pos="j" xml:id="A62714-001-a-0630">strange</w>
      <pc xml:id="A62714-001-a-0640">,</pc>
     </l>
     <l xml:id="A62714-e110">
      <w lemma="that" pos="cs" xml:id="A62714-001-a-0650">That</w>
      <w lemma="strike" pos="vvz" xml:id="A62714-001-a-0660">strikes</w>
      <w lemma="uneven" pos="j" xml:id="A62714-001-a-0670">uneven</w>
      <w lemma="like" pos="acp" xml:id="A62714-001-a-0680">like</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-0690">a</w>
      <w lemma="world" pos="n1" xml:id="A62714-001-a-0700">World</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0710">of</w>
      <w lemma="change" pos="n1" xml:id="A62714-001-a-0720">Change</w>
      <pc xml:id="A62714-001-a-0730">,</pc>
     </l>
     <l xml:id="A62714-e120">
      <w lemma="with" pos="acp" xml:id="A62714-001-a-0740">With</w>
      <w lemma="such" pos="d" xml:id="A62714-001-a-0750">such</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-0760">a</w>
      <w lemma="bold" pos="j" xml:id="A62714-001-a-0770">Bold</w>
      <w lemma="surprise" pos="n1" reg="Surprise" xml:id="A62714-001-a-0780">Surprize</w>
      <w lemma="attack" pos="n2" xml:id="A62714-001-a-0790">attacks</w>
      <w lemma="my" pos="po" xml:id="A62714-001-a-0800">my</w>
      <w lemma="sense" pos="n1" xml:id="A62714-001-a-0810">Sense</w>
      <pc xml:id="A62714-001-a-0820">,</pc>
     </l>
     <l xml:id="A62714-e130">
      <w lemma="beyond" pos="acp" xml:id="A62714-001-a-0830">Beyond</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-0840">the</w>
      <w lemma="power" pos="n1" xml:id="A62714-001-a-0850">Power</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-0860">of</w>
      <w lemma="counsel" pos="n1" xml:id="A62714-001-a-0870">Counsel</w>
      <w lemma="or" pos="cc" xml:id="A62714-001-a-0880">or</w>
      <w lemma="defence" pos="n1" xml:id="A62714-001-a-0890">Defence</w>
      <pc unit="sentence" xml:id="A62714-001-a-0900">?</pc>
     </l>
     <l xml:id="A62714-e140">
      <w lemma="but" pos="acp" xml:id="A62714-001-a-0910">But</w>
      <w lemma="though" pos="cs" xml:id="A62714-001-a-0920">tho'</w>
      <w lemma="blind" pos="j" xml:id="A62714-001-a-0930">Blind</w>
      <w lemma="fortune" pos="n1" xml:id="A62714-001-a-0940">Fortune</w>
      <w lemma="roll" pos="vvz" xml:id="A62714-001-a-0950">rolls</w>
      <w lemma="her" pos="po" xml:id="A62714-001-a-0960">her</w>
      <w lemma="turn" pos="j-vg" xml:id="A62714-001-a-0970">turning</w>
      <w lemma="wheel" pos="n1" xml:id="A62714-001-a-0980">Wheel</w>
     </l>
     <l xml:id="A62714-e150">
      <w lemma="with" pos="acp" xml:id="A62714-001-a-0990">With</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-1000">a</w>
      <w lemma="perpetual" pos="j" xml:id="A62714-001-a-1010">perpetual</w>
      <w lemma="motion" pos="n1" xml:id="A62714-001-a-1020">Motion</w>
      <pc xml:id="A62714-001-a-1030">,</pc>
      <w lemma="who" pos="crq" xml:id="A62714-001-a-1040">who</w>
      <w lemma="can" pos="vmb" xml:id="A62714-001-a-1050">can</w>
      <w lemma="feel" pos="vvi" xml:id="A62714-001-a-1060">feel</w>
     </l>
     <l xml:id="A62714-e160">
      <w lemma="this" pos="d" xml:id="A62714-001-a-1070">This</w>
      <w lemma="surge" pos="n1" xml:id="A62714-001-a-1080">Surge</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-1090">of</w>
      <w lemma="fate" pos="n1" xml:id="A62714-001-a-1100">Fate</w>
      <pc xml:id="A62714-001-a-1110">,</pc>
      <w lemma="push" pos="vvd" reg="pushed" xml:id="A62714-001-a-1120">push't</w>
      <w lemma="on" pos="acp" xml:id="A62714-001-a-1130">on</w>
      <w lemma="with" pos="acp" xml:id="A62714-001-a-1140">with</w>
      <w lemma="fire" pos="n1" xml:id="A62714-001-a-1150">Fire</w>
      <w lemma="and" pos="cc" xml:id="A62714-001-a-1160">and</w>
      <w lemma="steel" pos="n1" xml:id="A62714-001-a-1170">Steel</w>
      <pc unit="sentence" xml:id="A62714-001-a-1180">?</pc>
     </l>
     <l xml:id="A62714-e170">
      <w lemma="you" pos="pn" xml:id="A62714-001-a-1190">You</w>
      <w lemma="precious" pos="j" xml:id="A62714-001-a-1200">precious</w>
      <w lemma="moment" pos="ng1" reg="Moment's" xml:id="A62714-001-a-1210">Moments</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-1220">of</w>
      <w lemma="Serener" pos="nn1" xml:id="A62714-001-a-1230">Serener</w>
      <w lemma="days" pos="nn1" xml:id="A62714-001-a-1240">Days</w>
      <pc unit="sentence" xml:id="A62714-001-a-1250">!</pc>
     </l>
     <l xml:id="A62714-e180">
      <w lemma="when" pos="crq" xml:id="A62714-001-a-1260">When</w>
      <w lemma="many" pos="d" xml:id="A62714-001-a-1270">many</w>
      <w lemma="victory" pos="n2" xml:id="A62714-001-a-1280">Victories</w>
      <w lemma="enlarge" pos="vvn" reg="enlarged" xml:id="A62714-001-a-1290">enlarg'd</w>
      <w lemma="my" pos="po" xml:id="A62714-001-a-1300">my</w>
      <w lemma="praise" pos="n1" xml:id="A62714-001-a-1310">Praise</w>
      <pc xml:id="A62714-001-a-1320">,</pc>
     </l>
     <l xml:id="A62714-e190">
      <w lemma="and" pos="cc" xml:id="A62714-001-a-1330">And</w>
      <w lemma="all" pos="d" xml:id="A62714-001-a-1340">all</w>
      <w lemma="thing" pos="n2" xml:id="A62714-001-a-1350">things</w>
      <w lemma="run" pos="vvd" xml:id="A62714-001-a-1360">ran</w>
      <w lemma="in" pos="acp" xml:id="A62714-001-a-1370">in</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-1380">a</w>
      <w lemma="most" pos="avs-d" xml:id="A62714-001-a-1390">most</w>
      <w lemma="easy" pos="j" reg="easy" xml:id="A62714-001-a-1400">easie</w>
      <w lemma="stream" pos="n1" xml:id="A62714-001-a-1410">Stream</w>
      <pc xml:id="A62714-001-a-1420">,</pc>
     </l>
     <l xml:id="A62714-e200">
      <w lemma="back" pos="av" xml:id="A62714-001-a-1430">Back</w>
      <w lemma="unto" pos="acp" xml:id="A62714-001-a-1440">unto</w>
      <w lemma="i" pos="pno" xml:id="A62714-001-a-1450">me</w>
      <w lemma="their" pos="po" xml:id="A62714-001-a-1460">their</w>
      <w lemma="ocean" pos="n1" xml:id="A62714-001-a-1470">Ocean</w>
      <w lemma="and" pos="cc" xml:id="A62714-001-a-1480">and</w>
      <w lemma="supreme" pos="j" reg="Supreme" xml:id="A62714-001-a-1490">Supream</w>
      <pc unit="sentence" xml:id="A62714-001-a-1500">.</pc>
     </l>
     <l xml:id="A62714-e210">
      <w lemma="be" pos="vvb" xml:id="A62714-001-a-1510">Are</w>
      <w lemma="you" pos="pn" xml:id="A62714-001-a-1520">you</w>
      <w lemma="all" pos="av-d" xml:id="A62714-001-a-1530">all</w>
      <w lemma="vanish" pos="vvn" reg="vanished" xml:id="A62714-001-a-1540">vanish'd</w>
      <w lemma="by" pos="acp" xml:id="A62714-001-a-1550">by</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-1560">the</w>
      <w lemma="sudden" pos="j" xml:id="A62714-001-a-1570">sudden</w>
      <w lemma="fright" pos="j" xml:id="A62714-001-a-1580">Fright</w>
      <pc xml:id="A62714-001-a-1590">,</pc>
     </l>
     <l xml:id="A62714-e220">
      <w lemma="and" pos="cc" xml:id="A62714-001-a-1600">And</w>
      <w lemma="leave" pos="vvd" xml:id="A62714-001-a-1610">left</w>
      <w lemma="i" pos="pno" reg="my" xml:id="A62714-001-a-1620">m'</w>
      <w lemma="encompass" pos="vvn" reg="encompassed" xml:id="A62714-001-a-1630">incompass'd</w>
      <w lemma="with" pos="acp" xml:id="A62714-001-a-1640">with</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-1650">a</w>
      <w lemma="dismal" pos="j" xml:id="A62714-001-a-1660">Dismal</w>
      <w lemma="night" pos="n1" xml:id="A62714-001-a-1670">Night</w>
      <pc unit="sentence" xml:id="A62714-001-a-1680">?</pc>
     </l>
     <l xml:id="A62714-e230">
      <w lemma="by" pos="acp" xml:id="A62714-001-a-1690">By</w>
      <w lemma="my" pos="po" xml:id="A62714-001-a-1700">my</w>
      <w lemma="own" pos="d" xml:id="A62714-001-a-1710">own</w>
      <w lemma="subject" pos="n2" xml:id="A62714-001-a-1720">Subjects</w>
      <w lemma="in" pos="acp" xml:id="A62714-001-a-1730">in</w>
      <w lemma="suspicion" pos="n1" xml:id="A62714-001-a-1740">suspicion</w>
      <w lemma="hold" pos="vvn" xml:id="A62714-001-a-1750">held</w>
      <pc xml:id="A62714-001-a-1760">,</pc>
     </l>
     <l xml:id="A62714-e240">
      <w lemma="murmur" pos="n2-vg" xml:id="A62714-001-a-1770">Murmurings</w>
      <pc xml:id="A62714-001-a-1780">,</pc>
      <w lemma="as" pos="acp" xml:id="A62714-001-a-1790">as</w>
      <w lemma="bad" pos="j" xml:id="A62714-001-a-1800">bad</w>
      <w lemma="as" pos="acp" xml:id="A62714-001-a-1810">as</w>
      <w lemma="if" pos="cs" xml:id="A62714-001-a-1820">if</w>
      <w lemma="they" pos="pns" xml:id="A62714-001-a-1830">they</w>
      <w lemma="have" pos="vvd" xml:id="A62714-001-a-1840">had</w>
      <w lemma="rebel" pos="vvd" reg="Rebelled" xml:id="A62714-001-a-1850">Rebell'd</w>
      <pc unit="sentence" xml:id="A62714-001-a-1860">?</pc>
     </l>
     <l xml:id="A62714-e250">
      <w lemma="you" pos="pn" xml:id="A62714-001-a-1870">You</w>
      <w lemma="all" pos="d" xml:id="A62714-001-a-1880">all</w>
      <w lemma="control" pos="vvg" reg="Controlling" xml:id="A62714-001-a-1890">Controuling</w>
      <w lemma="power" pos="n2" xml:id="A62714-001-a-1900">Powers</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-1910">of</w>
      <w lemma="thing" pos="n2" xml:id="A62714-001-a-1920">things</w>
      <w lemma="above" pos="acp" xml:id="A62714-001-a-1930">above</w>
      <pc unit="sentence" xml:id="A62714-001-a-1940">!</pc>
     </l>
     <l xml:id="A62714-e260">
      <w lemma="who" pos="crq" xml:id="A62714-001-a-1950">Whose</w>
      <w lemma="easy" pos="jc" xml:id="A62714-001-a-1960">easier</w>
      <w lemma="dictate" pos="vvz" xml:id="A62714-001-a-1970">Dictates</w>
      <w lemma="guide" pos="vvi" xml:id="A62714-001-a-1980">guide</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-1990">the</w>
      <w lemma="world" pos="n1" xml:id="A62714-001-a-2000">World</w>
      <w lemma="by" pos="acp" xml:id="A62714-001-a-2010">by</w>
      <w lemma="love" pos="n1" xml:id="A62714-001-a-2020">Love</w>
      <pc unit="sentence" xml:id="A62714-001-a-2030">!</pc>
     </l>
     <l xml:id="A62714-e270">
      <w lemma="avert" pos="vvi" xml:id="A62714-001-a-2040">Avert</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-2050">th'</w>
      <w lemma="impendent" pos="j" xml:id="A62714-001-a-2060">Impendent</w>
      <w lemma="misery" pos="n2" xml:id="A62714-001-a-2070">Miseries</w>
      <pc xml:id="A62714-001-a-2080">,</pc>
      <w lemma="and" pos="cc" xml:id="A62714-001-a-2090">and</w>
      <w lemma="show" pos="vvi" xml:id="A62714-001-a-2100">show</w>
     </l>
     <l xml:id="A62714-e280">
      <w lemma="we" pos="pno" xml:id="A62714-001-a-2110">Us</w>
      <w lemma="earthly" pos="j" xml:id="A62714-001-a-2120">Earthly</w>
      <w lemma="god" pos="n2" xml:id="A62714-001-a-2130">Gods</w>
      <w lemma="to" pos="prt" xml:id="A62714-001-a-2140">to</w>
      <w lemma="govern" pos="vvi" xml:id="A62714-001-a-2150">Govern</w>
      <w lemma="here" pos="av" xml:id="A62714-001-a-2160">here</w>
      <w lemma="below" pos="acp" xml:id="A62714-001-a-2170">below</w>
      <pc unit="sentence" xml:id="A62714-001-a-2180">!</pc>
     </l>
    </lg>
    <lg xml:id="A62714-e290">
     <head xml:id="A62714-e300">
      <w lemma="the" pos="d" xml:id="A62714-001-a-2190">The</w>
      <w lemma="answer" pos="n1" xml:id="A62714-001-a-2200">Answer</w>
      <pc unit="sentence" xml:id="A62714-001-a-2210">.</pc>
     </head>
     <l xml:id="A62714-e310">
      <w join="right" lemma="it" pos="pn" xml:id="A62714-001-a-2220">'T</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A62714-001-a-2221">IS</w>
      <w lemma="well" pos="av" xml:id="A62714-001-a-2230">well</w>
      <w join="right" lemma="you" pos="pn" xml:id="A62714-001-a-2240">you</w>
      <w join="left" lemma="have" pos="vvb" xml:id="A62714-001-a-2241">'ve</w>
      <w lemma="think" pos="vvn" xml:id="A62714-001-a-2250">thought</w>
      <w lemma="upon" pos="acp" xml:id="A62714-001-a-2260">upon</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-2270">the</w>
      <w lemma="chief" pos="js" xml:id="A62714-001-a-2280">chiefest</w>
      <w lemma="cause" pos="n1" xml:id="A62714-001-a-2290">Cause</w>
     </l>
     <l xml:id="A62714-e320">
      <w lemma="change" pos="vvb" xml:id="A62714-001-a-2300">Change</w>
      <w lemma="nothing" pos="pix" xml:id="A62714-001-a-2310">nothing</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-2320">of</w>
      <w lemma="religion" pos="n1" xml:id="A62714-001-a-2330">Religion</w>
      <w lemma="nor" pos="ccx" xml:id="A62714-001-a-2340">nor</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-2350">the</w>
      <w lemma="law" pos="n2" xml:id="A62714-001-a-2360">Laws</w>
      <pc xml:id="A62714-001-a-2370">,</pc>
     </l>
     <l xml:id="A62714-e330">
      <w lemma="and" pos="cc" xml:id="A62714-001-a-2380">And</w>
      <w lemma="then" pos="av" xml:id="A62714-001-a-2390">then</w>
      <w lemma="no" pos="dx" xml:id="A62714-001-a-2400">no</w>
      <w lemma="discontent" pos="n1" xml:id="A62714-001-a-2410">Discontent</w>
      <w lemma="shall" pos="vmb" xml:id="A62714-001-a-2420">shall</w>
      <w lemma="here" pos="av" xml:id="A62714-001-a-2430">here</w>
      <w lemma="invade" pos="vvi" xml:id="A62714-001-a-2440">Invade</w>
      <pc xml:id="A62714-001-a-2450">,</pc>
     </l>
     <l xml:id="A62714-e340">
      <w lemma="for" pos="acp" xml:id="A62714-001-a-2460">For</w>
      <w lemma="thief" pos="n2" reg="Thiefs" xml:id="A62714-001-a-2470">Thieves</w>
      <w lemma="will" pos="vmd" xml:id="A62714-001-a-2480">would</w>
      <w lemma="enter" pos="vvi" xml:id="A62714-001-a-2490">enter</w>
      <w lemma="out" pos="av" xml:id="A62714-001-a-2500">out</w>
      <w lemma="of" pos="acp" xml:id="A62714-001-a-2510">of</w>
      <w lemma="a" pos="d" xml:id="A62714-001-a-2520">a</w>
      <w lemma="bravade" pos="vvb" xml:id="A62714-001-a-2530">Bravade</w>
      <pc xml:id="A62714-001-a-2540">,</pc>
     </l>
     <l xml:id="A62714-e350">
      <w lemma="to" pos="prt" xml:id="A62714-001-a-2550">To</w>
      <w lemma="rectify" pos="vvi" xml:id="A62714-001-a-2560">rectify</w>
      <w lemma="all" pos="d" xml:id="A62714-001-a-2570">all</w>
      <w lemma="wrong" pos="n2-j" xml:id="A62714-001-a-2580">wrongs</w>
      <pc xml:id="A62714-001-a-2590">,</pc>
      <w lemma="when" pos="crq" xml:id="A62714-001-a-2600">when</w>
      <w lemma="their" pos="po" xml:id="A62714-001-a-2610">their</w>
      <w lemma="intent" pos="n1" xml:id="A62714-001-a-2620">Intent</w>
     </l>
     <l xml:id="A62714-e360">
      <w lemma="be" pos="vvz" xml:id="A62714-001-a-2630">'S</w>
      <w lemma="themselves" pos="pr" xml:id="A62714-001-a-2640">themselves</w>
      <w lemma="to" pos="prt" xml:id="A62714-001-a-2650">t'</w>
      <w lemma="enrich" pos="vvi" reg="enrich" xml:id="A62714-001-a-2660">inrich</w>
      <pc xml:id="A62714-001-a-2670">,</pc>
      <w lemma="and" pos="cc" xml:id="A62714-001-a-2680">and</w>
      <w lemma="not" pos="xx" xml:id="A62714-001-a-2690">not</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-2700">the</w>
      <w lemma="government" pos="n1" xml:id="A62714-001-a-2710">Government</w>
      <pc unit="sentence" xml:id="A62714-001-a-2720">.</pc>
     </l>
     <l xml:id="A62714-e370">
      <w lemma="let" pos="vvb" xml:id="A62714-001-a-2730">Let</w>
      <w lemma="the" pos="d" xml:id="A62714-001-a-2740">the</w>
      <w lemma="great" pos="j" xml:id="A62714-001-a-2750">Great</w>
      <w lemma="monarch" pos="n1" xml:id="A62714-001-a-2760">Monarch</w>
      <w lemma="this" pos="d" xml:id="A62714-001-a-2770">this</w>
      <w lemma="good" pos="j" xml:id="A62714-001-a-2780">good</w>
      <w lemma="motto" pos="n1" xml:id="A62714-001-a-2790">Motto</w>
      <w lemma="wear" pos="vvi" xml:id="A62714-001-a-2800">wear</w>
      <pc xml:id="A62714-001-a-2810">,</pc>
     </l>
     <l xml:id="A62714-e380">
      <w lemma="not" pos="xx" xml:id="A62714-001-a-2820">Not</w>
      <w lemma="only" pos="av-j" xml:id="A62714-001-a-2830">only</w>
      <w lemma="in" pos="acp" xml:id="A62714-001-a-2840">in</w>
      <w lemma="his" pos="po" xml:id="A62714-001-a-2850">his</w>
      <w lemma="arm" pos="n2" xml:id="A62714-001-a-2860">Arms</w>
      <w lemma="but" pos="acp" xml:id="A62714-001-a-2870">but</w>
      <w lemma="every" pos="d" xml:id="A62714-001-a-2880">every</w>
      <w lemma="where" pos="crq" xml:id="A62714-001-a-2890">where</w>
      <pc unit="sentence" xml:id="A62714-001-a-2900">.</pc>
     </l>
     <l xml:id="A62714-e390">
      <foreign xml:id="A62714-e400" xml:lang="lat">
       <w lemma="integer" pos="fla" xml:id="A62714-001-a-2910">Integer</w>
       <w lemma="vitæ" pos="fla" xml:id="A62714-001-a-2920">Vitae</w>
      </foreign>
      <pc rend="follows-hi" xml:id="A62714-001-a-2930">,</pc>
      <w lemma="be" pos="vvz" xml:id="A62714-001-a-2940">is</w>
      <w lemma="my" pos="po" xml:id="A62714-001-a-2950">my</w>
      <w lemma="whole" pos="j" xml:id="A62714-001-a-2960">whole</w>
      <w lemma="defence" pos="n1" xml:id="A62714-001-a-2970">Defence</w>
      <pc xml:id="A62714-001-a-2980">,</pc>
     </l>
     <l xml:id="A62714-e410">
      <foreign xml:id="A62714-e420" xml:lang="lat">
       <w lemma="scelerisque" pos="fla" xml:id="A62714-001-a-2990">Scelerisque</w>
       <w lemma="purus" pos="fla" xml:id="A62714-001-a-3000">purus</w>
      </foreign>
      <pc rend="follows-hi" xml:id="A62714-001-a-3010">,</pc>
      <w lemma="a" pos="d" xml:id="A62714-001-a-3020">a</w>
      <w lemma="most" pos="avs-d" xml:id="A62714-001-a-3030">most</w>
      <w lemma="strong" pos="j" xml:id="A62714-001-a-3040">strong</w>
      <w lemma="defence" pos="n1" xml:id="A62714-001-a-3050">Defence</w>
      <pc xml:id="A62714-001-a-3060">;</pc>
     </l>
     <l xml:id="A62714-e430">
      <foreign xml:id="A62714-e440" xml:lang="lat">
       <w lemma="non" pos="fla" xml:id="A62714-001-a-3070">Non</w>
       <w lemma="eget" pos="fla" xml:id="A62714-001-a-3080">eget</w>
       <w lemma="maurus" pos="fla" xml:id="A62714-001-a-3090">Mauri</w>
      </foreign>
      <pc rend="follows-hi" xml:id="A62714-001-a-3100">,</pc>
      <w lemma="that" pos="cs" xml:id="A62714-001-a-3110">that</w>
      <w lemma="no" pos="dx" xml:id="A62714-001-a-3120">no</w>
      <w lemma="force" pos="n2" xml:id="A62714-001-a-3130">Forces</w>
      <w lemma="need" pos="vvb" xml:id="A62714-001-a-3140">need</w>
      <pc xml:id="A62714-001-a-3150">,</pc>
     </l>
     <l xml:id="A62714-e450">
      <foreign xml:id="A62714-e460" xml:lang="lat">
       <w lemma="iaculum" pos="fla" xml:id="A62714-001-a-3160">Jaculis</w>
       <w lemma="nec" pos="fla" xml:id="A62714-001-a-3170">nec</w>
       <w lemma="arcu" pos="fla" xml:id="A62714-001-a-3180">Arcu</w>
      </foreign>
      <pc rend="follows-hi" xml:id="A62714-001-a-3190">,</pc>
      <w lemma="which" pos="crq" xml:id="A62714-001-a-3200">which</w>
      <w lemma="contention" pos="n2" xml:id="A62714-001-a-3210">Contentions</w>
      <w lemma="breed" pos="vvb" xml:id="A62714-001-a-3220">breed</w>
      <pc xml:id="A62714-001-a-3230">:</pc>
     </l>
     <l xml:id="A62714-e470">
      <hi xml:id="A62714-e480">
       <w lemma="nec" pos="fla" xml:id="A62714-001-a-3240">Nec</w>
       <w lemma="venenatis" pos="fla" xml:id="A62714-001-a-3250">venenatis</w>
       <w lemma="gravida" pos="fla" xml:id="A62714-001-a-3260">gravida</w>
       <w lemma="sagittis" pos="fla" xml:id="A62714-001-a-3270">Sagittis</w>
      </hi>
     </l>
     <l xml:id="A62714-e490">
      <w lemma="pharetra" pos="fla" rend="hi" xml:id="A62714-001-a-3280">Pharetra</w>
      <pc xml:id="A62714-001-a-3290">,</pc>
      <w lemma="to" pos="prt" xml:id="A62714-001-a-3300">to</w>
      <w lemma="make" pos="vvi" xml:id="A62714-001-a-3310">make</w>
      <w lemma="loyal" pos="j" xml:id="A62714-001-a-3320">Loyal</w>
      <w lemma="his" pos="po" xml:id="A62714-001-a-3330">his</w>
      <w lemma="own" pos="d" xml:id="A62714-001-a-3340">own</w>
      <w lemma="city" pos="n2" xml:id="A62714-001-a-3350">Cities</w>
      <pc unit="sentence" xml:id="A62714-001-a-3360">.</pc>
     </l>
    </lg>
    <trailer xml:id="A62714-e510">
     <w lemma="finis" pos="fla" xml:id="A62714-001-a-3370">FINIS</w>
     <pc unit="sentence" xml:id="A62714-001-a-3380">.</pc>
    </trailer>
   </div>
  </body>
 </text>
</TEI>
